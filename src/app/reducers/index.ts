import * as fromRouter from '@ngrx/router-store';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
/**
 * storeFreeze prevents state from being mutated. When mutation occurs, an
 * exception will be thrown. This is useful during development mode to
 * ensure that none of the reducers accidentally mutates the state.
 */
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';
import * as fromBarns from '../core/reducers/barns.reducer';
import * as fromCows from '../core/reducers/cows.reducer';
import * as fromFarms from '../core/reducers/farms.reducer';
import * as fromSectors from '../core/reducers/sectors.reducer';
import * as fromWallpoints from '../core/reducers/wallpoints.reducer';

export interface State {
  router: fromRouter.RouterReducerState;
  barns: fromBarns.State;
  farms: fromFarms.State;
  wallpoints: fromWallpoints.State;
  sectors: fromSectors.State;
  cows: fromCows.State;
}

export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer,
  barns: fromBarns.reducer,
  farms: fromFarms.reducer,
  wallpoints: fromWallpoints.reducer,
  sectors: fromSectors.reducer,
  cows: fromCows.reducer
};

// console.log all actions
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state: State, action: any): any => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return result;
  };
}

export const metaReducers: Array<MetaReducer<State>> = !environment.production
  ? [logger, storeFreeze]
  : [];

// Barns
export const getBarnsState = createFeatureSelector<State, fromBarns.State>('barns');

export const getSelectedBarnId = createSelector(
  getBarnsState,
  fromBarns.getSelectedId
);

export const getBarnsLoadingStatus = createSelector(
  getBarnsState,
  fromBarns.getLoadingStatus
);

export const getBarnsError = createSelector(
  getBarnsState,
  fromBarns.getError
);

export const {
  selectIds: getBarnIds,
  selectEntities: getBarnEntities,
  selectAll: getAllBarns,
  selectTotal: getTotalBarns
} = fromBarns.adapter.getSelectors(getBarnsState);

export const getSelectedBarn = createSelector(
  getBarnEntities,
  getSelectedBarnId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

// Farms
export const getFarmsState = createFeatureSelector<State, fromFarms.State>('farms');

export const getSelectedFarmId = createSelector(
  getFarmsState,
  fromFarms.getSelectedId
);

export const getFarmsLoadingStatus = createSelector(
  getFarmsState,
  fromFarms.getLoadingStatus
);

export const getFarmsError = createSelector(
  getFarmsState,
  fromFarms.getError
);

export const {
  selectIds: getFarmIds,
  selectEntities: getFarmEntities,
  selectAll: getAllFarms,
  selectTotal: getTotalFarms
} = fromFarms.adapter.getSelectors(getFarmsState);

export const getSelectedFarm = createSelector(
  getFarmEntities,
  getSelectedFarmId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

// Wallpoints
export const getWallpointsState = createFeatureSelector<State, fromWallpoints.State>('wallpoints');

export const getSelectedWallpointId = createSelector(
  getWallpointsState,
  fromWallpoints.getSelectedId
);

export const getWallpointsLoadingStatus = createSelector(
  getWallpointsState,
  fromWallpoints.getLoadingStatus
);

export const getWallpointsError = createSelector(
  getWallpointsState,
  fromWallpoints.getError
);

export const {
  selectIds: getWallpointIds,
  selectEntities: getWallpointEntities,
  selectAll: getAllWallpoints,
  selectTotal: getTotalWallpoint
} = fromWallpoints.adapter.getSelectors(getWallpointsState);

export const getSelectedWallpoint = createSelector(
  getWallpointEntities,
  getSelectedWallpointId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

// Sectors
export const getSectorsState = createFeatureSelector<State, fromSectors.State>('sectors');

export const getSelectedSectorId = createSelector(
  getSectorsState,
  fromSectors.getSelectedId
);

export const getSectorsLoadingStatus = createSelector(
  getSectorsState,
  fromSectors.getLoadingStatus
);

export const getSectorsError = createSelector(
  getSectorsState,
  fromSectors.getError
);

export const {
  selectIds: getSectorIds,
  selectEntities: getSectorEntities,
  selectAll: getAllSectors,
  selectTotal: getTotalSector
} = fromSectors.adapter.getSelectors(getSectorsState);

export const getSelectedSector = createSelector(
  getSectorEntities,
  getSelectedSectorId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

// Cows
export const getCowsState = createFeatureSelector<State, fromCows.State>('cows');

export const getSelectedCowId = createSelector(
  getCowsState,
  fromCows.getSelectedId
);

export const getCowsLoadingStatus = createSelector(
  getCowsState,
  fromCows.getLoadingStatus
);

export const getCowsError = createSelector(
  getCowsState,
  fromCows.getError
);

export const {
  selectIds: getCowIds,
  selectEntities: getCowEntities,
  selectAll: getAllCows,
  selectTotal: getTotalCow
} = fromCows.adapter.getSelectors(getCowsState);

export const getSelectedCow = createSelector(
  getCowEntities,
  getSelectedCowId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

export const getLocateResponse = createSelector(
  getCowsState,
  fromCows.getLocateResponse
);
