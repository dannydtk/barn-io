import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {
  FarmActionsUnion,
  FarmActionTypes,
  FetchError,
  FetchSuccess
} from '../actions/farm.actions';
import { FarmService } from '../services/farm.service';

@Injectable()
export class FarmEffects {
  @Effect()
  fetch$: Observable<Action> = this.actions$.pipe(
    ofType<FarmActionsUnion>(FarmActionTypes.FETCH),
    mergeMap(() =>
      this.farmService.getAllFarms().pipe(
        map(farms => new FetchSuccess(farms)),
        catchError(error => of(new FetchError(error)))
      )
    )
  );

  constructor(private actions$: Actions, private farmService: FarmService) {}
}
