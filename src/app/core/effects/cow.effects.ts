import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, select, Store } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, mergeMap, take } from 'rxjs/operators';
import * as fromRoot from '../../reducers';
import {
  CowActionsUnion,
  CowActionTypes,
  FetchError,
  FetchSuccess,
  LocateComplete,
  LocateError
} from '../actions/cow.actions';
import { LocateResponse } from '../models/LocateResponse';
import { CowService } from '../services/cow.service';

@Injectable()
export class CowEffects {
  @Effect()
  fetch$: Observable<Action> = this.actions$.pipe(
    ofType<CowActionsUnion>(CowActionTypes.FETCH),
    mergeMap(action =>
      this.cowService.getFarmCows(action.payload).pipe(
        map(cows => new FetchSuccess(cows)),
        catchError(error => of(new FetchError(error)))
      )
    )
  );

  @Effect()
  locate$: Observable<Action> = this.actions$.pipe(
    ofType<CowActionsUnion>(CowActionTypes.LOCATE),
    mergeMap(action =>
      forkJoin([
        this.cowService.locate(action.payload).pipe(
          map(locateResponse => new LocateComplete(locateResponse)),
          catchError(error => of(new LocateError(error)))
        ),
        this.store.pipe(
          select(fromRoot.getSelectedFarmId),
          take(1)
        ),
        this.store.pipe(
          select(fromRoot.getAllBarns),
          take(1)
        )
      ]).pipe(
        map(([locateAction, farmId, barns]) => {
          if (locateAction.payload) {
            const locateResponse: LocateResponse = locateAction.payload;
            const barnId: string = barns.find(barn => barn.id === locateResponse[0].barnId).id;
            this.router.navigate(['manager', farmId, barnId], { queryParams: { locate: true } });
          }
          return locateAction;
        })
      )
    )
  );

  constructor(
    private actions$: Actions,
    private cowService: CowService,
    private router: Router,
    private store: Store<fromRoot.State>
  ) {}
}
