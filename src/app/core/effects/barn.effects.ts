import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {
  BarnActionsUnion,
  BarnActionTypes,
  FetchError,
  FetchSuccess
} from '../actions/barn.actions';
import { BarnService } from '../services/barn.service';

@Injectable()
export class BarnEffects {
  @Effect()
  fetch$: Observable<Action> = this.actions$.pipe(
    ofType<BarnActionsUnion>(BarnActionTypes.FETCH),
    mergeMap(action =>
      this.barnService.getFarmBarns(action.payload).pipe(
        map(barns => new FetchSuccess(barns)),
        catchError(error => of(new FetchError(error)))
      )
    )
  );

  constructor(private actions$: Actions, private barnService: BarnService) {}
}
