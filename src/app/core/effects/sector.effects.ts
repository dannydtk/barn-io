import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {
  FetchError,
  FetchSuccess,
  SectorActionsUnion,
  SectorActionTypes
} from '../actions/sector.actions';
import { SectorService } from '../services/sector.service';

@Injectable()
export class SectorEffects {
  @Effect()
  fetch$: Observable<Action> = this.actions$.pipe(
    ofType<SectorActionsUnion>(SectorActionTypes.FETCH),
    mergeMap(action =>
      this.sectorService.getBarnSectors(action.payload).pipe(
        map(sectors => new FetchSuccess(sectors)),
        catchError(error => of(new FetchError(error)))
      )
    )
  );

  constructor(private actions$: Actions, private sectorService: SectorService) {}
}
