import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {
  FetchError,
  FetchSuccess,
  WallpointActionsUnion,
  WallpointActionTypes
} from '../actions/wallpoint.actions';
import { WallpointService } from '../services/wallpoint.service';

@Injectable()
export class WallpointEffects {
  @Effect()
  fetch$: Observable<Action> = this.actions$.pipe(
    ofType<WallpointActionsUnion>(WallpointActionTypes.FETCH),
    mergeMap(action =>
      this.wallpointService.getBarnWallpoints(action.payload).pipe(
        map(wallpoints => new FetchSuccess(wallpoints)),
        catchError(error => of(new FetchError(error)))
      )
    )
  );

  constructor(private actions$: Actions, private wallpointService: WallpointService) {}
}
