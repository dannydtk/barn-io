import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromRoot from '../../../reducers';
import * as FarmActions from '../../actions/farm.actions';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  constructor(private store: Store<fromRoot.State>, private router: Router) {}

  get selectedFarmId$(): Observable<number> {
    return this.store.pipe(select(fromRoot.getSelectedFarmId));
  }

  onFarmSelectNavigate(): void {
    this.store.dispatch(new FarmActions.ResetSelection());
    this.router.navigate(['manager']);
  }
}
