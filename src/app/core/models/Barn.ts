export interface Barn {
  id: string;
  number: number;
  width: number;
  height: number;
  name?: string;
}
