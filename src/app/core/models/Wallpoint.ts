export interface WallpointData {
  id: number;
  barnId: number;
  mac: string;
  name?: string;
  x: number;
  y: number;
  number?: number;
}
