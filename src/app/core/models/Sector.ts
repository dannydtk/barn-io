export interface SectorData {
  id: number;
  barnId: number;
  number: number;
  x1: number;
  y1: number;
  x2: number;
  y2: number;
}
