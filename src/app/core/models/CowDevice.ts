export interface CowDevice {
  id: string;
  mac: string;
  name: string;
  number: number;
}
