export interface CowData {
  id: string;
  farmId: string;
  number: number;
}
