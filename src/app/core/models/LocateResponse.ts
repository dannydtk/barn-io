export interface LocateResult {
  barnId: string;
  sectorNumber: number;
  probability: number;
}

export interface LocateResponse extends Array<LocateResult> {}
