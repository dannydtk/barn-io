import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as fromRoot from '../../reducers';

@Injectable({
  providedIn: 'root'
})
export class FarmChosenGuard implements CanActivate {
  constructor(private store: Store<fromRoot.State>, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.pipe(
      select(fromRoot.getSelectedFarmId),
      take(1),
      map(val => {
        if (!val) {
          this.router.navigate(['/']);
          return false;
        }
        return true;
      })
    );
  }
}
