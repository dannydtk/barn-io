import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as FarmActions from '../../../core/actions/farm.actions';
import * as fromRoot from '../../../reducers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'barn-io';

  constructor(private store: Store<fromRoot.State>) {}

  ngOnInit(): void {
    this.store.dispatch(new FarmActions.Fetch());
  }
}
