import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CowData } from '../models/Cow';
import { LocateResponse } from '../models/LocateResponse';

@Injectable({
  providedIn: 'root'
})
export class CowService {
  constructor(private http: HttpClient) {}

  getFarmCows(farmId: number): Observable<CowData[]> {
    return this.http.get<CowData[]>(`http://localhost:3000/farms/${farmId}/cows`);
  }

  locate(_id: number): Observable<LocateResponse> {
    return this.http.get<LocateResponse>(`http://localhost:3000/locate/`);
  }
}
