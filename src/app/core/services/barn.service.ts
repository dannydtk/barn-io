import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Barn } from '../models/Barn';

@Injectable({
  providedIn: 'root'
})
export class BarnService {
  constructor(private http: HttpClient) {}

  getFarmBarns(farmId: number): Observable<Barn[]> {
    return this.http.get<Barn[]>(`http://localhost:3000/farms/${farmId}/barns`);
  }
}
