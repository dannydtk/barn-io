import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WallpointData } from '../models/Wallpoint';

@Injectable({
  providedIn: 'root'
})
export class WallpointService {
  constructor(private http: HttpClient) {}

  getBarnWallpoints(barnId: number): Observable<WallpointData[]> {
    return this.http.get<WallpointData[]>(`http://localhost:3000/barns/${barnId}/wallpoints`);
  }
}
