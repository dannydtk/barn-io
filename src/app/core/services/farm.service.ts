import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Farm } from '../models/Farm';

@Injectable({
  providedIn: 'root'
})
export class FarmService {
  constructor(private http: HttpClient) {}

  getAllFarms(): Observable<Farm[]> {
    return this.http.get<Farm[]>('http://localhost:3000/farms');
  }
}
