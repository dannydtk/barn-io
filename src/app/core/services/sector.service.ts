import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SectorData } from '../models/Sector';

@Injectable({
  providedIn: 'root'
})
export class SectorService {
  constructor(private http: HttpClient) {}

  getBarnSectors(barnId: number): Observable<SectorData[]> {
    return this.http.get<SectorData[]>(`http://localhost:3000/barns/${barnId}/sectors`);
  }
}
