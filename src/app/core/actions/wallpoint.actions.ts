import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
import { WallpointData } from '../models/Wallpoint';

export enum WallpointActionTypes {
  FETCH = '[Wallpoint] Fetch',
  FETCH_SUCCESS = '[Wallpoint] Fetch Success',
  FETCH_ERROR = '[Wallpoint] Fetch Error',
  ADD = '[Wallpoint] Add',
  REMOVE = '[Wallpoint] Remove',
  EDIT = '[Wallpoint] Edit',
  SELECT = '[Wallpoint] Select',
  RESET = '[Wallpoint] Reset'
}

export class Fetch implements Action {
  readonly type = WallpointActionTypes.FETCH;

  constructor(public payload: number) {}
}

export class FetchSuccess implements Action {
  readonly type = WallpointActionTypes.FETCH_SUCCESS;

  constructor(public payload: WallpointData[]) {}
}

export class FetchError implements Action {
  readonly type = WallpointActionTypes.FETCH_ERROR;

  constructor(public payload: any) {}
}

export class Add implements Action {
  readonly type = WallpointActionTypes.ADD;

  constructor(public payload: WallpointData) {}
}

export class Edit implements Action {
  readonly type = WallpointActionTypes.EDIT;

  constructor(public payload: Update<WallpointData>) {}
}

export class Remove implements Action {
  readonly type = WallpointActionTypes.REMOVE;

  constructor(public payload: string) {}
}

export class Select implements Action {
  readonly type = WallpointActionTypes.SELECT;

  constructor(public payload: number) {}
}

export class Reset implements Action {
  readonly type = WallpointActionTypes.RESET;

  constructor(public payload?: any) {}
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type WallpointActionsUnion =
  | Fetch
  | FetchSuccess
  | FetchError
  | Add
  | Edit
  | Remove
  | Select
  | Reset;
