import { Action } from '@ngrx/store';
import { CowData } from '../models/Cow';
import { LocateResponse } from '../models/LocateResponse';

export enum CowActionTypes {
  FETCH = '[Cow] Fetch',
  FETCH_SUCCESS = '[Cow] Fetch Success',
  FETCH_ERROR = '[Cow] Fetch Error',
  LOCATE = '[Cow] Locate',
  LOCATE_COMPLETE = '[Cow] Locate Complete',
  LOCATE_ERROR = '[Cow] Locate Error',
  SELECT = '[Cow] Select',
  RESET = '[Cow] Reset'
}

export class Fetch implements Action {
  readonly type = CowActionTypes.FETCH;

  constructor(public payload: number) {}
}

export class FetchSuccess implements Action {
  readonly type = CowActionTypes.FETCH_SUCCESS;

  constructor(public payload: CowData[]) {}
}

export class FetchError implements Action {
  readonly type = CowActionTypes.FETCH_ERROR;

  constructor(public payload: any) {}
}

export class Locate implements Action {
  readonly type = CowActionTypes.LOCATE;

  constructor(public payload: number) {}
}

export class LocateComplete implements Action {
  readonly type = CowActionTypes.LOCATE_COMPLETE;

  constructor(public payload: LocateResponse) {}
}

export class LocateError implements Action {
  readonly type = CowActionTypes.LOCATE_ERROR;

  constructor(public payload: any) {}
}

export class Select implements Action {
  readonly type = CowActionTypes.SELECT;

  constructor(public payload: number) {}
}

export class Reset implements Action {
  readonly type = CowActionTypes.RESET;

  constructor(public payload?: any) {}
}

export type CowActionsUnion =
  | Fetch
  | FetchSuccess
  | FetchError
  | Locate
  | LocateComplete
  | LocateError
  | Select
  | Reset;
