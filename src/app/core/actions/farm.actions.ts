import { Action } from '@ngrx/store';
import { Farm } from '../models/Farm';

export enum FarmActionTypes {
  FETCH = '[Farm] Fetch',
  FETCH_SUCCESS = '[Farm] Fetch Success',
  FETCH_ERROR = '[Farm] Fetch Error',
  SELECT = '[Farm] Select',
  RESET_SELECTION = '[Farm] Reset Selection'
}

export class Fetch implements Action {
  readonly type = FarmActionTypes.FETCH;
}

export class FetchSuccess implements Action {
  readonly type = FarmActionTypes.FETCH_SUCCESS;

  constructor(public payload: Farm[]) {}
}

export class FetchError implements Action {
  readonly type = FarmActionTypes.FETCH_ERROR;

  constructor(public payload: any) {}
}

export class Select implements Action {
  readonly type = FarmActionTypes.SELECT;

  constructor(public payload: number) {}
}

export class ResetSelection implements Action {
  readonly type = FarmActionTypes.RESET_SELECTION;

  constructor(public payload?: any) {}
}

export type FarmActionsUnion = Fetch | FetchSuccess | FetchError | Select | ResetSelection;
