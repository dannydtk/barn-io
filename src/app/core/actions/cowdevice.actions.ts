import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
import { CowDevice } from '../models/CowDevice';

export enum CowDeviceActionTypes {
  Search = '[CowDevice] Search',
  SearchComplete = '[CowDevice] Search Complete',
  SearchError = '[CowDevice] Search Error',
  Load = '[CowDevice] Load',
  Add = '[CowDevice] Add',
  Remove = '[CowDevice] Remove',
  Edit = '[CowDevice] Edit',
  Select = '[CowDevice] Select'
}

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class Search implements Action {
  readonly type = CowDeviceActionTypes.Search;

  constructor(public payload: string) {}
}

export class SearchComplete implements Action {
  readonly type = CowDeviceActionTypes.SearchComplete;

  constructor(public payload: CowDevice[]) {}
}

export class SearchError implements Action {
  readonly type = CowDeviceActionTypes.SearchError;

  constructor(public payload: string) {}
}

export class Load implements Action {
  readonly type = CowDeviceActionTypes.Load;

  constructor(public payload: CowDevice) {}
}

export class Edit implements Action {
  readonly type = CowDeviceActionTypes.Edit;

  constructor(public payload: Update<CowDevice>) {}
}

export class Remove implements Action {
  readonly type = CowDeviceActionTypes.Remove;

  constructor(public payload: string) {}
}

export class Select implements Action {
  readonly type = CowDeviceActionTypes.Select;

  constructor(public payload: string) {}
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type CowDeviceActionsUnion = Search | SearchComplete | SearchError | Load | Edit | Remove | Select;
