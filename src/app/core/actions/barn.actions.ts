import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
import { Barn } from '../models/Barn';

export enum BarnActionTypes {
  FETCH = '[Barn] Fetch',
  FETCH_SUCCESS = '[Barn] Fetch Success',
  FETCH_ERROR = '[Barn] Fetch Error',
  ADD = '[Barn] Add',
  ADD_BATCH = '[Barn] Add Batch',
  REMOVE = '[Barn] Remove',
  EDIT = '[Barn] Edit',
  SELECT = '[Barn] Select',
  RESET = '[Barn] Reset'
}
export class Fetch implements Action {
  readonly type = BarnActionTypes.FETCH;

  constructor(public payload: number) {}
}

export class FetchSuccess implements Action {
  readonly type = BarnActionTypes.FETCH_SUCCESS;

  constructor(public payload: Barn[]) {}
}

export class FetchError implements Action {
  readonly type = BarnActionTypes.FETCH_ERROR;

  constructor(public payload: any) {}
}

export class Add implements Action {
  readonly type = BarnActionTypes.ADD;

  constructor(public payload: Barn) {}
}

export class AddBatch implements Action {
  readonly type = BarnActionTypes.ADD_BATCH;

  constructor(public payload: Barn[]) {}
}

export class Remove implements Action {
  readonly type = BarnActionTypes.REMOVE;

  constructor(public payload: string) {}
}

export class Edit implements Action {
  readonly type = BarnActionTypes.EDIT;

  constructor(public payload: Update<Barn>) {}
}

export class Select implements Action {
  readonly type = BarnActionTypes.SELECT;

  constructor(public payload: number) {}
}

export class Reset implements Action {
  readonly type = BarnActionTypes.RESET;

  constructor(public payload?: any) {}
}

export type BarnActionsUnion =
  | Fetch
  | FetchSuccess
  | FetchError
  | Add
  | AddBatch
  | Remove
  | Edit
  | Select
  | Reset;
