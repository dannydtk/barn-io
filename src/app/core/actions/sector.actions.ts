import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
import { SectorData } from '../models/Sector';

export enum SectorActionTypes {
  FETCH = '[Sector] Fetch',
  FETCH_SUCCESS = '[Sector] Fetch Success',
  FETCH_ERROR = '[Sector] Fetch Error',
  ADD = '[Sector] Add',
  REMOVE = '[Sector] Remove',
  EDIT = '[Sector] Edit',
  SELECT = '[Sector] Select',
  RESET = '[Sector] Reset'
}

export class Fetch implements Action {
  readonly type = SectorActionTypes.FETCH;

  constructor(public payload: number) {}
}

export class FetchSuccess implements Action {
  readonly type = SectorActionTypes.FETCH_SUCCESS;

  constructor(public payload: SectorData[]) {}
}

export class FetchError implements Action {
  readonly type = SectorActionTypes.FETCH_ERROR;

  constructor(public payload: any) {}
}

export class Add implements Action {
  readonly type = SectorActionTypes.ADD;

  constructor(public payload: SectorData) {}
}

export class Edit implements Action {
  readonly type = SectorActionTypes.EDIT;

  constructor(public payload: Update<SectorData>) {}
}

export class Remove implements Action {
  readonly type = SectorActionTypes.REMOVE;

  constructor(public payload: string) {}
}

export class Select implements Action {
  readonly type = SectorActionTypes.SELECT;

  constructor(public payload: number) {}
}

export class Reset implements Action {
  readonly type = SectorActionTypes.RESET;

  constructor(public payload?: any) {}
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type SectorActionsUnion =
  | Fetch
  | FetchSuccess
  | FetchError
  | Add
  | Edit
  | Remove
  | Select
  | Reset;
