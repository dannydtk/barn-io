import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { MaterialModule } from '../material/material.module';
import { HeaderComponent } from './components/header/header.component';
import { NavComponent } from './components/nav/nav.component';
import { BarnEffects } from './effects/barn.effects';
import { CowEffects } from './effects/cow.effects';
import { FarmEffects } from './effects/farm.effects';
import { SectorEffects } from './effects/sector.effects';
import { WallpointEffects } from './effects/wallpoint.effects';
import { AppComponent } from './pages/app/app.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    EffectsModule.forFeature([
      BarnEffects,
      FarmEffects,
      WallpointEffects,
      SectorEffects,
      CowEffects
    ])
  ],
  declarations: [AppComponent, HeaderComponent, NavComponent],
  exports: [AppComponent, HeaderComponent, NavComponent]
})
export class CoreModule {}
