import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { CowActionsUnion, CowActionTypes } from '../actions/cow.actions';
import { CowData } from '../models/Cow';
import { LocateResponse } from '../models/LocateResponse';

export interface State extends EntityState<CowData> {
  selectedCowId: number | null;
  locateResponse: LocateResponse | null;
  loading: boolean;
  error: any | null;
}

export const adapter: EntityAdapter<CowData> = createEntityAdapter<CowData>({
  selectId: (cow: CowData) => cow.id,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  selectedCowId: null,
  locateResponse: null,
  loading: false,
  error: null
});

export function reducer(state = initialState, action: CowActionsUnion): State {
  switch (action.type) {
    case CowActionTypes.FETCH: {
      return {
        ...state,
        loading: true
      };
    }

    case CowActionTypes.FETCH_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loading: false,
        error: null
      };
    }

    case CowActionTypes.FETCH_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }

    case CowActionTypes.SELECT: {
      return {
        ...state,
        selectedCowId: action.payload
      };
    }

    case CowActionTypes.LOCATE: {
      return {
        ...state,
        loading: true,
        error: null
      };
    }

    case CowActionTypes.LOCATE_COMPLETE: {
      return {
        ...state,
        locateResponse: action.payload,
        loading: false,
        error: null
      };
    }

    case CowActionTypes.LOCATE_ERROR: {
      return {
        ...state,
        locateResponse: null,
        loading: false,
        error: action.payload
      };
    }

    case CowActionTypes.RESET: {
      return {
        ...state,
        ...adapter.removeAll(state),
        selectedCowId: null
      };
    }

    default: {
      return state;
    }
  }
}

export const getSelectedId = (state: State) => state.selectedCowId;
export const getLoadingStatus = (state: State) => state.loading;
export const getError = (state: State) => state.error;
export const getLocateResponse = (state: State) => state.locateResponse;
