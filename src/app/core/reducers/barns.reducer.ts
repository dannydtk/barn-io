import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { BarnActionsUnion, BarnActionTypes } from '../actions/barn.actions';
import { Barn } from '../models/Barn';

export interface State extends EntityState<Barn> {
  selectedBarnId: number | null;
  loading: boolean;
  error: any | null;
}

export const adapter: EntityAdapter<Barn> = createEntityAdapter<Barn>({
  selectId: (barn: Barn) => barn.id,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  selectedBarnId: null,
  loading: false,
  error: null
});

export function reducer(state = initialState, action: BarnActionsUnion): State {
  switch (action.type) {
    case BarnActionTypes.FETCH: {
      return {
        ...state,
        loading: true
      };
    }

    case BarnActionTypes.FETCH_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loading: false,
        error: null
      };
    }

    case BarnActionTypes.FETCH_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }

    case BarnActionTypes.ADD: {
      return adapter.addOne(action.payload, state);
    }

    case BarnActionTypes.ADD_BATCH: {
      return adapter.addMany(action.payload, state);
    }

    case BarnActionTypes.EDIT: {
      return adapter.updateOne(action.payload, state);
    }

    case BarnActionTypes.REMOVE: {
      return adapter.removeOne(action.payload, state);
    }

    case BarnActionTypes.SELECT: {
      return {
        ...state,
        selectedBarnId: action.payload
      };
    }

    case BarnActionTypes.RESET: {
      return {
        ...state,
        ...adapter.removeAll(state),
        selectedBarnId: null
      };
    }

    default: {
      return state;
    }
  }
}

export const getSelectedId = (state: State) => state.selectedBarnId;
export const getLoadingStatus = (state: State) => state.loading;
export const getError = (state: State) => state.error;
