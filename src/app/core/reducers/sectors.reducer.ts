import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { SectorActionsUnion, SectorActionTypes } from '../actions/sector.actions';
import { SectorData } from '../models/Sector';

export interface State extends EntityState<SectorData> {
  selectedSectorId: number | null;
  loading: boolean;
  error: any | null;
}

export const adapter: EntityAdapter<SectorData> = createEntityAdapter<SectorData>({
  selectId: (Sector: SectorData) => Sector.id,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  selectedSectorId: null,
  loading: false,
  error: null
});

export function reducer(state = initialState, action: SectorActionsUnion): State {
  switch (action.type) {
    case SectorActionTypes.FETCH: {
      return {
        ...state,
        loading: true
      };
    }

    case SectorActionTypes.FETCH_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loading: false,
        error: null
      };
    }

    case SectorActionTypes.FETCH_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }

    case SectorActionTypes.ADD: {
      return adapter.addOne(action.payload, state);
    }

    case SectorActionTypes.EDIT: {
      return adapter.updateOne(action.payload, state);
    }

    case SectorActionTypes.SELECT: {
      return {
        ...state,
        selectedSectorId: action.payload
      };
    }

    case SectorActionTypes.RESET: {
      return {
        ...state,
        ...adapter.removeAll(state),
        selectedSectorId: null
      };
    }

    default: {
      return state;
    }
  }
}

export const getSelectedId = (state: State) => state.selectedSectorId;
export const getLoadingStatus = (state: State) => state.loading;
export const getError = (state: State) => state.error;
