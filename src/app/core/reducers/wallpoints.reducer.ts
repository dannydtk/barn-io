import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { WallpointActionsUnion, WallpointActionTypes } from '../actions/wallpoint.actions';
import { WallpointData } from '../models/Wallpoint';

export interface State extends EntityState<WallpointData> {
  selectedWallpointId: number | null;
  loading: boolean;
  error: any | null;
}

export const adapter: EntityAdapter<WallpointData> = createEntityAdapter<WallpointData>({
  selectId: (wallpoint: WallpointData) => wallpoint.id,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  selectedWallpointId: null,
  loading: false,
  error: null
});

export function reducer(state = initialState, action: WallpointActionsUnion): State {
  switch (action.type) {
    case WallpointActionTypes.FETCH: {
      return {
        ...state,
        loading: true
      };
    }

    case WallpointActionTypes.FETCH_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loading: false,
        error: null
      };
    }

    case WallpointActionTypes.FETCH_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }

    case WallpointActionTypes.ADD: {
      return adapter.addOne(action.payload, state);
    }

    case WallpointActionTypes.EDIT: {
      return adapter.updateOne(action.payload, state);
    }

    case WallpointActionTypes.SELECT: {
      return {
        ...state,
        selectedWallpointId: action.payload
      };
    }

    case WallpointActionTypes.RESET: {
      return {
        ...state,
        ...adapter.removeAll(state),
        selectedWallpointId: null
      };
    }

    default: {
      return state;
    }
  }
}

export const getSelectedId = (state: State) => state.selectedWallpointId;
export const getLoadingStatus = (state: State) => state.loading;
export const getError = (state: State) => state.error;
