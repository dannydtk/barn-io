import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { FarmActionsUnion, FarmActionTypes } from '../actions/farm.actions';
import { Farm } from '../models/Farm';

export interface State extends EntityState<Farm> {
  selectedFarmId: number | null;
  loading: boolean;
  error: any | null;
}

export const adapter: EntityAdapter<Farm> = createEntityAdapter<Farm>({
  selectId: (farm: Farm) => farm.id,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  selectedFarmId: null,
  loading: false,
  error: null
});

export function reducer(state = initialState, action: FarmActionsUnion): State {
  switch (action.type) {
    case FarmActionTypes.FETCH: {
      return {
        ...state,
        loading: true
      };
    }

    case FarmActionTypes.FETCH_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loading: false,
        error: null
      };
    }

    case FarmActionTypes.FETCH_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }

    case FarmActionTypes.SELECT: {
      return {
        ...state,
        selectedFarmId: action.payload
      };
    }

    case FarmActionTypes.RESET_SELECTION: {
      return {
        ...state,
        selectedFarmId: null
      };
    }

    default: {
      return state;
    }
  }
}

export const getSelectedId = (state: State) => state.selectedFarmId;
export const getLoadingStatus = (state: State) => state.loading;
export const getError = (state: State) => state.error;
