import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BarnManagerComponent } from './pages/barn-manager/barn-manager.component';
import { BarnsOverviewComponent } from './pages/barns-overview/barns-overview.component';
import { FarmSelectComponent } from './pages/farm-select/farm-select.component';

const routes: Routes = [
  {
    path: '',
    component: FarmSelectComponent
  },
  {
    path: ':farmId',
    component: BarnsOverviewComponent
  },
  {
    path: ':farmId/:barnId',
    component: BarnManagerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule {}
