import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ToolActionsUnion, ToolActionTypes } from '../actions/tool.actions';
import { ElementName } from '../models/Element';
import { Tool, ToolType } from '../models/Tool';

export interface State extends EntityState<Tool> {
  selectedToolId: string | null;
}

export const adapter: EntityAdapter<Tool> = createEntityAdapter<Tool>({
  selectId: (tool: Tool) => `${tool.name}-${tool.type}`,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  selectedToolId: null,
  ids: ['draw-start', 'wallpoint-add', 'cowdevice-add'],
  entities: {
    'draw-start': {
      name: ElementName.DRAW,
      type: ToolType.START,
      iconName: 'draw',
      color: '#000',
      active: true
    },
    'wallpoint-add': {
      name: ElementName.WALLPOINT,
      type: ToolType.ADD,
      iconName: 'bluetooth',
      color: '#0a3d91',
      active: true
    },
    'cowdevice-add': {
      name: ElementName.COWDEVICE,
      type: ToolType.ADD,
      iconName: 'bluetooth',
      color: '#00963e',
      active: false
    }
  }
});

export function reducer(state = initialState, action: ToolActionsUnion): State {
  switch (action.type) {
    case ToolActionTypes.SELECT: {
      return {
        ...state,
        selectedToolId: action.payload
      };
    }
    case ToolActionTypes.RESET: {
      return {
        ...state,
        selectedToolId: null
      };
    }

    default: {
      return state;
    }
  }
}

export const getSelectedId = (state: State) => state.selectedToolId;
