import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromElements from './elements.reducer';
import * as fromTools from './tools.reducer';

export interface ManagerState {
  tools: fromTools.State;
  elements: fromElements.State;
}

export interface State extends fromRoot.State {
  manager: ManagerState;
}

export const reducers: ActionReducerMap<ManagerState> = {
  tools: fromTools.reducer,
  elements: fromElements.reducer
};

export const getManagerState = createFeatureSelector<State, ManagerState>('manager');

export const getToolsEntitiesState = createSelector(
  getManagerState,
  state => state.tools
);

export const getSelectedToolId = createSelector(
  getToolsEntitiesState,
  fromTools.getSelectedId
);

export const {
  selectIds: getToolIds,
  selectEntities: getToolEntities,
  selectAll: getAllTools,
  selectTotal: getTotalTools
} = fromTools.adapter.getSelectors(getToolsEntitiesState);

export const getElementsState = createSelector(
  getManagerState,
  state => state.elements
);

export const {
  selectIds: getElementIds,
  selectEntities: getElementEntities,
  selectAll: getAllElements,
  selectTotal: getTotalElements
} = fromElements.adapter.getSelectors(getElementsState);
