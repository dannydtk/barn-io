import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action } from '@ngrx/store';
import { Element, ElementName } from '../models/Element';

export interface State extends EntityState<Element> {}

export const adapter: EntityAdapter<Element> = createEntityAdapter<Element>({
  selectId: (element: Element) => `${element.name}`,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  ids: ['wallpoint', 'collar'],
  entities: {
    wallpoint: { name: ElementName.WALLPOINT, iconName: 'bluetooth', color: '#0a3d91' },
    collar: { name: ElementName.COWDEVICE, iconName: 'bluetooth', color: '#00963e' }
  }
});

export function reducer(state = initialState, action: Action): State {
  switch (action.type) {
    default: {
      return state;
    }
  }
}
