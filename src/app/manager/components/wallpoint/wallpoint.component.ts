import { Component, Input } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map, take } from 'rxjs/operators';
import { WallpointData } from '../../../core/models/Wallpoint';
import { Element, ElementName, ElementState } from '../../models/Element';
import * as fromManager from '../../reducers';

@Component({
  selector: 'app-wallpoint',
  templateUrl: './wallpoint.component.html',
  styleUrls: ['./wallpoint.component.scss']
})
export class WallpointComponent {
  @Input() data: WallpointData;

  x = 0;
  y = 0;

  element: Element;

  get state(): ElementState {
    if (this.data && this.data.mac) {
      return ElementState.ASSIGNED;
    }

    return ElementState.UNASSIGNED;
  }

  get mac(): string {
    return this.data && this.data.mac ? this.data.mac : 'Nieprzypisany';
  }

  constructor(private store: Store<fromManager.State>) {
    this.store
      .pipe(
        select(fromManager.getElementEntities),
        map(dictionary => dictionary[ElementName.WALLPOINT]),
        take(1)
      )
      .subscribe(element => (this.element = element));
  }
}
