import { Component, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';

@Component({
  selector: 'app-instructions-snackbar',
  templateUrl: './instructions-snackbar.component.html',
  styleUrls: ['./instructions-snackbar.component.scss']
})
export class InstructionsSnackbarComponent implements OnInit {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public message: string) {}

  ngOnInit() {}
}
