import {
  AfterViewInit,
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Renderer2,
  SimpleChanges,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { MatDialog, MatSnackBar, MatSnackBarRef } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, take, withLatestFrom } from 'rxjs/operators';
import { LocateResult } from 'src/app/core/models/LocateResponse';
import * as fromRoot from 'src/app/reducers';
import { RxjsComponent } from 'src/app/shared/classes/RxjsComponent';
import { ElementsHostDirective } from 'src/app/shared/directives/elements-host.directive';
import * as SectorActions from '../../../core/actions/sector.actions';
import * as WallpointActions from '../../../core/actions/wallpoint.actions';
import { Barn } from '../../../core/models/Barn';
import { SectorData } from '../../../core/models/Sector';
import { WallpointData } from '../../../core/models/Wallpoint';
import * as ToolActions from '../../actions/tool.actions';
import { ToolEffects } from '../../effects/tool.effects';
import { ElementName, ElementState } from '../../models/Element';
import { Position } from '../../models/Position';
import { ToolType } from '../../models/Tool';
import * as fromManager from '../../reducers';
import { CowDeviceComponent } from '../cow-device/cow-device.component';
import { EditSectorModalComponent } from '../edit-sector-modal/edit-sector-modal.component';
import { EditWallpointModalComponent } from '../edit-wallpoint-modal/edit-wallpoint-modal.component';
import { InstructionsSnackbarComponent } from '../instructions-snackbar/instructions-snackbar.component';
import { SectorComponent } from '../sector/sector.component';
import { WallpointComponent } from '../wallpoint/wallpoint.component';

@Component({
  selector: 'app-barn',
  templateUrl: './barn.component.html',
  styleUrls: ['./barn.component.scss']
})
export class BarnComponent extends RxjsComponent
  implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() barn: Barn;
  @ViewChild(ElementsHostDirective) elementsHost: ElementsHostDirective;
  @ViewChild('barnElement') barnElement: ElementRef;

  isDrawToolSelected$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  wallpointFactory: ComponentFactory<WallpointComponent>;
  collarFactory: ComponentFactory<CowDeviceComponent>;
  sectorFactory: ComponentFactory<SectorComponent>;

  viewContainerRef: ViewContainerRef;
  snackbarRef: MatSnackBarRef<InstructionsSnackbarComponent> = null;

  wallpointElements: ComponentRef<WallpointComponent>[] = [];
  cowDeviceElements: ComponentRef<CowDeviceComponent>[] = [];
  sectorElements: ComponentRef<SectorComponent>[] = [];

  startPoint$: Subject<Position> = new Subject<Position>();
  endPoint$: Subject<Position> = new Subject<Position>();

  private listeners: (() => void)[] = [];

  @HostListener('window:resize')
  onWindowResize(): void {
    this.setWallpointsPosition();
    this.setSectorsPosition();
  }

  constructor(
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private renderer: Renderer2,
    private toolEffects: ToolEffects,
    private componentFactoryResolver: ComponentFactoryResolver,
    private store: Store<fromManager.State>,
    private route: ActivatedRoute
  ) {
    super();
    this.wallpointFactory = this.componentFactoryResolver.resolveComponentFactory(
      WallpointComponent
    );
    this.collarFactory = this.componentFactoryResolver.resolveComponentFactory(CowDeviceComponent);
    this.sectorFactory = this.componentFactoryResolver.resolveComponentFactory(SectorComponent);
  }

  ngOnInit(): void {
    this.addSubscription(
      this.toolEffects.select$.subscribe(toolId => {
        switch (toolId) {
          case `${ElementName.WALLPOINT}-${ToolType.ADD}`:
            this.addWallpoint();
            break;
          case `${ElementName.DRAW}-${ToolType.START}`:
            this.isDrawToolSelected$.next(true);
            break;
          default:
            this.isDrawToolSelected$.next(false);
            break;
        }
      })
    );
    this.addSubscription(
      this.endPoint$
        .pipe(
          withLatestFrom(this.startPoint$),
          withLatestFrom(this.isDrawToolSelected$)
        )
        .subscribe(([points, isDrawToolSelected]) => {
          if (isDrawToolSelected) {
            this.addSector(null, points);
            this.isDrawToolSelected$.next(false);
          }
        })
    );
    this.addSubscription(
      this.isDrawToolSelected$.subscribe(value => {
        if (value) {
          this.openSnackBar('Przeciągnij, aby narysować sektor');
        } else if (this.snackbarRef) {
          this.snackbarRef.dismiss();
        }
      })
    );
    this.addSubscription(
      this.store
        .pipe(
          select(fromRoot.getWallpointsLoadingStatus),
          filter(isLoading => !isLoading),
          withLatestFrom(this.store.pipe(select(fromRoot.getAllWallpoints)))
        )
        .subscribe(([, wallpoints]) => {
          wallpoints.forEach(wallpointData => {
            this.addWallpoint(wallpointData);
          });
          this.setWallpointsPosition();
        })
    );
    this.addSubscription(
      this.store
        .pipe(
          select(fromRoot.getSectorsLoadingStatus),
          filter(isLoading => !isLoading),
          withLatestFrom(this.store.pipe(select(fromRoot.getAllSectors)))
        )
        .subscribe(([, sectors]) => {
          sectors.forEach(sectorData => {
            this.addSector(sectorData);
          });
        })
    );
    this.addSubscription(
      this.store
        .pipe(
          select(fromRoot.getSectorsLoadingStatus),
          filter(isLoading => !isLoading),
          withLatestFrom(
            this.store.pipe(
              select(fromRoot.getLocateResponse),
              filter(locateResponse => !!locateResponse)
            ),
            this.route.queryParams
          ),
          filter(([, , queryParams]) => Boolean(queryParams.locate))
        )
        .subscribe(([, locateResponse]) => {
          locateResponse.forEach(locateResult => {
            this.markLocation(locateResult);
          });
        })
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.barn) {
      this.reset();
    }
  }

  ngAfterViewInit(): void {
    this.setWallpointsPosition();
  }

  createWallpoints(): void {
    this.store
      .pipe(
        select(fromRoot.getAllWallpoints),
        take(1)
      )
      .subscribe(wallpoints => {
        wallpoints.forEach(wallpointData => {
          this.addWallpoint(wallpointData);
        });
      });
  }

  addWallpoint(data?: WallpointData): void {
    const wallpoint = this.viewContainerRef.createComponent(this.wallpointFactory);
    if (data) {
      wallpoint.instance.data = data;
    } else {
      this.store.dispatch(new ToolActions.Reset());
    }

    this.listeners.push(
      this.renderer.listen(wallpoint.location.nativeElement, 'dblclick', () =>
        this.openWallpointEditModal(wallpoint)
      )
    );

    this.renderer.setAttribute(wallpoint.location.nativeElement, 'cdkDragBoundary', '.inner');

    this.wallpointElements.push(wallpoint);
  }

  addSector(data?: SectorData, points?: [Position, Position]): void {
    const sector = this.viewContainerRef.createComponent(this.sectorFactory);
    if (data) {
      sector.instance.data = data;

      const startPoint: Position = this.calculatePixelPosition({
        x: sector.instance.data.x1,
        y: sector.instance.data.y1
      });
      const endPoint: Position = this.calculatePixelPosition({
        x: sector.instance.data.x2,
        y: sector.instance.data.y2
      });

      sector.instance.width = Math.abs(startPoint.x - endPoint.x);
      sector.instance.height = Math.abs(startPoint.y - endPoint.y);
      this.renderer.setStyle(
        sector.location.nativeElement,
        'transform',
        `translate3d(${startPoint.x}px, ${startPoint.y}px, 0px)`
      );
    } else if (points) {
      sector.instance.width = Math.abs(points[0].x - points[1].x);
      sector.instance.height = Math.abs(points[0].y - points[1].y);
      this.renderer.setStyle(
        sector.location.nativeElement,
        'transform',
        `translate3d(${(points[0].x < points[1].x ? points[0].x : points[1].x) -
          this.barnSizes.width / 2}px, ${-(points[0].y < points[1].y ? points[1].y : points[0].y) +
          this.barnSizes.height / 2}px, 0px)`
      );
      this.store.dispatch(new ToolActions.Reset());
    }

    this.listeners.push(
      this.renderer.listen(sector.location.nativeElement, 'dblclick', () =>
        this.openSectorEditModal(sector)
      )
    );
    this.sectorElements.push(sector);
  }

  setWallpointsPosition(): void {
    this.wallpointElements.forEach(wallpoint => {
      const { x, y } = this.calculatePixelPosition({
        x: wallpoint.instance.data.x,
        y: wallpoint.instance.data.y
      });
      wallpoint.instance.x = x;
      wallpoint.instance.y = y;
      this.renderer.setStyle(
        wallpoint.location.nativeElement,
        'transform',
        `translate3d(${wallpoint.instance.x}px, ${wallpoint.instance.y}px, 0px)`
      );
    });
  }

  setSectorsPosition(): void {
    this.sectorElements.forEach(sector => {
      const startPoint = this.calculatePixelPosition({
        x: sector.instance.data.x1,
        y: sector.instance.data.y1
      });
      sector.instance.x1 = startPoint.x;
      sector.instance.y1 = startPoint.y;
      const endPoint = this.calculatePixelPosition({
        x: sector.instance.data.x2,
        y: sector.instance.data.y2
      });
      sector.instance.x2 = endPoint.x;
      sector.instance.y2 = endPoint.y;
      this.renderer.setStyle(
        sector.location.nativeElement,
        'transform',
        `translate3d(${startPoint.x}px, ${startPoint.y}px, 0px)`
      );
      sector.instance.width = Math.abs(startPoint.x - endPoint.x);
      sector.instance.height = Math.abs(startPoint.y - endPoint.y);
    });
  }

  markLocation(locateResult: LocateResult): void {
    const foundSector = this.sectorElements.find(
      sectorElement => sectorElement.instance.data.number === locateResult.sectorNumber
    );

    if (foundSector) {
      foundSector.instance.probability = locateResult.probability;
    }
  }

  calculatePixelPosition(position: Position): Position {
    return {
      x: (this.barnSizes.width * position.x) / this.barn.width - this.barnSizes.width / 2,
      y: this.barnSizes.height / 2 - (this.barnSizes.height * position.y) / this.barn.height
    };
  }

  openWallpointEditModal(wallpoint: ComponentRef<WallpointComponent>): void {
    const modalRef = this.dialog.open(EditWallpointModalComponent, {
      width: '300px',
      data: wallpoint.instance.data || {}
    });

    modalRef.afterClosed().subscribe(result => {
      if (result) {
        switch (result.action) {
          case 'remove': {
            this.wallpointElements = this.wallpointElements.filter(wp => wp !== wallpoint);
            wallpoint.destroy();
            break;
          }
          case 'update': {
            wallpoint.instance.data = {
              ...wallpoint.instance.data,
              ...result.payload
            };
            if (result.flags.update) {
              this.store.dispatch(
                new WallpointActions.Edit({
                  id: wallpoint.instance.data.id,
                  changes: wallpoint.instance.data
                })
              );
            } else {
              this.store
                .pipe(
                  select(fromRoot.getWallpointIds),
                  take(1)
                )
                .subscribe(wallpointIds => {
                  wallpoint.instance.data = {
                    ...wallpoint.instance.data,
                    id: Number(wallpointIds[wallpointIds.length - 1]) + 1,
                    barnId: Number(this.barn.id)
                  };
                  this.store.dispatch(new WallpointActions.Add(wallpoint.instance.data));
                });
            }
            break;
          }
          default:
            break;
        }
      }
    });
  }

  openSectorEditModal(sector: ComponentRef<SectorComponent>): void {
    const modalRef = this.dialog.open(EditSectorModalComponent, {
      width: '300px',
      data: { ...sector.instance.data, feedback: Boolean(sector.instance.probability) } || {}
    });

    modalRef.afterClosed().subscribe(result => {
      if (result) {
        switch (result.action) {
          case 'remove': {
            this.sectorElements = this.sectorElements.filter(sec => sec !== sector);
            sector.destroy();
            break;
          }
          case 'update': {
            sector.instance.data = {
              ...sector.instance.data,
              ...result.payload
            };
            if (result.flags.update) {
              this.store.dispatch(
                new SectorActions.Edit({
                  id: sector.instance.data.id,
                  changes: sector.instance.data
                })
              );
            } else {
              this.store
                .pipe(
                  select(fromRoot.getSectorIds),
                  take(1)
                )
                .subscribe(sectorIds => {
                  sector.instance.data = {
                    ...sector.instance.data,
                    id: Number(sectorIds[sectorIds.length - 1]) + 1,
                    barnId: Number(this.barn.id)
                  };
                  this.store.dispatch(new SectorActions.Add(sector.instance.data));
                });
            }
            break;
          }
          default:
            break;
        }
      }
    });
  }

  openSnackBar(instruction: string, duration?: number): void {
    this.snackbarRef = this.snackBar.openFromComponent(InstructionsSnackbarComponent, {
      data: instruction,
      verticalPosition: 'top',
      duration
    });
  }

  onMouseDown(event: MouseEvent): void {
    this.startPoint$.next({
      x: event.clientX - this.barnSizes.left,
      y: this.barnSizes.bottom - event.clientY
    });
  }

  onMouseUp(event: MouseEvent): void {
    this.endPoint$.next({
      x: event.clientX - this.barnSizes.left,
      y: this.barnSizes.bottom - event.clientY
    });
  }

  get barnSizes(): any {
    return this.barnElement.nativeElement.getBoundingClientRect();
  }

  ngOnDestroy(): void {
    this.listeners.forEach(unlistenFunc => unlistenFunc());
  }

  reset(): void {
    this.wallpointElements.forEach(el => el.destroy());
    this.sectorElements.forEach(el => el.destroy());
    this.wallpointElements = [];
    this.sectorElements = [];
    this.viewContainerRef = this.elementsHost.viewContainerRef;
  }

  save(): void {
    if (
      this.wallpointElements.some(wallpoint => wallpoint.instance.state === ElementState.UNASSIGNED)
    ) {
      this.openSnackBar('Przynajmniej jedno urzadzenie naścienne nie ma przypisanych danych', 4000);
      return;
    }

    if (this.sectorElements.some(sector => sector.instance.state === ElementState.UNASSIGNED)) {
      this.openSnackBar('Przynajmniej jeden sektor nie ma przypisanych danych', 4000);
      return;
    }
  }

  get aspectRatio(): number {
    return this.barn.height / this.barn.width;
  }
}
