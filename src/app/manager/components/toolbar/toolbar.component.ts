import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as ToolActions from '../../actions/tool.actions';
import { Tool } from '../../models/Tool';
import * as fromManager from '../../reducers';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  tools$: Observable<Tool[]>;

  constructor(private store: Store<fromManager.State>) {}

  ngOnInit(): void {
    this.tools$ = this.store.pipe(
      select(fromManager.getAllTools),
      map(tools => tools.filter(tool => tool.active))
    );
  }

  onToolClicked(tool: Tool): void {
    this.store.dispatch(new ToolActions.Select(`${tool.name}-${tool.type}`));
  }
}
