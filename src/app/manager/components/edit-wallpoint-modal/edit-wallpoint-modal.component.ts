import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { EditWallpointModalData } from '../../models/EditWallpointModalData';

@Component({
  selector: 'app-edit-wallpoint-modal',
  templateUrl: './edit-wallpoint-modal.component.html',
  styleUrls: ['./edit-wallpoint-modal.component.scss']
})
export class EditWallpointModalComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditWallpointModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditWallpointModalData,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      mac: [this.data.mac || ''],
      number: [this.data.number || null],
      name: [this.data.name || '']
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onRemoveClick(): void {
    this.dialogRef.close({ action: 'remove' });
  }

  onOkClick(): void {
    this.dialogRef.close({
      action: 'update',
      payload: this.form.value,
      flags: { update: Boolean(this.data.id) }
    });
  }
}
