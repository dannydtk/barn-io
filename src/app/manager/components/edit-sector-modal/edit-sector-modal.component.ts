import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { EditSectorModalData } from '../../models/EditSectorModalData';

@Component({
  selector: 'app-edit-sector-modal',
  templateUrl: './edit-sector-modal.component.html',
  styleUrls: ['./edit-sector-modal.component.scss']
})
export class EditSectorModalComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditSectorModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditSectorModalData,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      number: [this.data.number || null]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onRemoveClick(): void {
    this.dialogRef.close({ action: 'remove' });
  }

  onOkClick(): void {
    this.dialogRef.close({
      action: 'update',
      payload: this.form.value,
      flags: { update: Boolean(this.data.id) }
    });
  }
}
