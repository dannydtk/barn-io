import { Component, Input, OnInit } from '@angular/core';
import { SectorData } from 'src/app/core/models/Sector';
import { ElementState } from '../../models/Element';

@Component({
  selector: 'app-sector',
  templateUrl: './sector.component.html',
  styleUrls: ['./sector.component.scss']
})
export class SectorComponent implements OnInit {
  @Input() data: SectorData;
  @Input() width = 0;
  @Input() height = 0;
  @Input() probability: number | null = null;

  x1 = 0;
  y1 = 0;
  x2 = 0;
  y2 = 0;

  get state(): ElementState {
    if (this.data && this.data.number) {
      return ElementState.ASSIGNED;
    }

    return ElementState.UNASSIGNED;
  }

  get probabilityColor(): string {
    if (this.probability && this.probability > 0) {
      if (this.probability < 0.33) {
        return 'rgba(255, 0, 0, 0.3)';
      } else if (this.probability >= 0.33 && this.probability <= 0.66) {
        return 'rgba(255, 255, 0, 0.3)';
      } else {
        return 'rgba(0, 255, 0, 0.3)';
      }
    } else {
      return 'none';
    }
  }

  px(val: number): string {
    return `${val}px`;
  }

  constructor() {}

  ngOnInit() {}
}
