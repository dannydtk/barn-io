import { Component, Input } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map, take } from 'rxjs/operators';
import { CowDevice } from 'src/app/core/models/CowDevice';
import { Element, ElementName } from '../../models/Element';
import * as fromManager from '../../reducers';

@Component({
  selector: 'app-cow-device',
  templateUrl: './cow-device.component.html',
  styleUrls: ['./cow-device.component.scss']
})
export class CowDeviceComponent {
  @Input()
  data: CowDevice;

  element: Element;

  constructor(private store: Store<fromManager.State>) {
    this.store
      .pipe(
        select(fromManager.getElementEntities),
        map(dictionary => dictionary[ElementName.COWDEVICE]),
        take(1)
      )
      .subscribe(element => (this.element = element));
  }
}
