import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, HostListener, Input } from '@angular/core';
import { Tool } from '../../models/Tool';

@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html',
  styleUrls: ['./tool.component.scss'],
  animations: [
    trigger('onHover', [
      state(
        'normal',
        style({
          transform: 'scale(1)',
          opacity: 1
        })
      ),
      state(
        'hovered',
        style({
          transform: 'scale(1.1)',
          opacity: 0.8
        })
      ),
      transition('normal <=> hovered', [animate('0.3s ease-in')])
    ])
  ]
})
export class ToolComponent {
  @Input()
  tool: Tool = null;

  isHovered = false;

  @HostListener('mouseenter')
  onMouseEnter() {
    this.isHovered = true;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.isHovered = false;
  }

  get hoverState(): string {
    return this.isHovered ? 'hovered' : 'normal';
  }

  constructor() {}
}
