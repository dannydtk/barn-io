import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { MaterialModule } from '../material/material.module';
import { DirectivesModule } from '../shared/directives/directives.module';
import { BarnComponent } from './components/barn/barn.component';
import { CowDeviceComponent } from './components/cow-device/cow-device.component';
import { EditSectorModalComponent } from './components/edit-sector-modal/edit-sector-modal.component';
import { EditWallpointModalComponent } from './components/edit-wallpoint-modal/edit-wallpoint-modal.component';
import { InstructionsSnackbarComponent } from './components/instructions-snackbar/instructions-snackbar.component';
import { SectorComponent } from './components/sector/sector.component';
import { ToolComponent } from './components/tool/tool.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { WallpointComponent } from './components/wallpoint/wallpoint.component';
import { ToolEffects } from './effects/tool.effects';
import { ManagerRoutingModule } from './manager-routing.module';
import { BarnManagerComponent } from './pages/barn-manager/barn-manager.component';
import { BarnsOverviewComponent } from './pages/barns-overview/barns-overview.component';
import { FarmSelectComponent } from './pages/farm-select/farm-select.component';
import { reducers } from './reducers';

@NgModule({
  imports: [
    CommonModule,
    ManagerRoutingModule,
    MaterialModule,
    HttpClientModule,
    AngularSvgIconModule,
    DirectivesModule,
    ReactiveFormsModule,
    StoreModule.forFeature('manager', reducers),
    EffectsModule.forFeature([ToolEffects])
  ],
  declarations: [
    BarnComponent,
    BarnManagerComponent,
    ToolbarComponent,
    ToolComponent,
    WallpointComponent,
    CowDeviceComponent,
    BarnsOverviewComponent,
    SectorComponent,
    InstructionsSnackbarComponent,
    EditWallpointModalComponent,
    EditSectorModalComponent,
    FarmSelectComponent
  ],
  entryComponents: [
    WallpointComponent,
    CowDeviceComponent,
    SectorComponent,
    InstructionsSnackbarComponent,
    EditWallpointModalComponent,
    EditSectorModalComponent
  ]
})
export class ManagerModule {}
