import { Action } from '@ngrx/store';

export enum ToolActionTypes {
  SELECT = '[Tool] Select',
  RESET = '[Tool] Reset selected tool'
}

export class Select implements Action {
  readonly type = ToolActionTypes.SELECT;

  constructor(public payload: string) {}
}

export class Reset implements Action {
  readonly type = ToolActionTypes.RESET;

  constructor(public payload: any = null) {}
}

export type ToolActionsUnion = Select | Reset;
