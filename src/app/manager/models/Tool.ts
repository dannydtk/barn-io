import { Element } from './Element';

export enum ToolType {
  ADD = 'add',
  START = 'start'
}

export interface Tool extends Element {
  type: ToolType;
  active: boolean;
}
