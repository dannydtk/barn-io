export enum ElementName {
  WALLPOINT = 'wallpoint',
  COWDEVICE = 'cowdevice',
  BARN = 'barn',
  DRAW = 'draw'
}

export enum ElementState {
  UNASSIGNED,
  ASSIGNED
}

export interface Element {
  name: ElementName;
  iconName: string;
  color?: string;
}
