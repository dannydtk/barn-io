import { SectorData } from 'src/app/core/models/Sector';

export interface EditSectorModalData extends SectorData {
  index: number;
  feedback: boolean;
}
