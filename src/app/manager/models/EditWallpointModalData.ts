import { WallpointData } from 'src/app/core/models/Wallpoint';

export interface EditWallpointModalData extends WallpointData {
  index: number;
}
