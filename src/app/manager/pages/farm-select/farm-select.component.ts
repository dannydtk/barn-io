import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Farm } from 'src/app/core/models/Farm';
import * as fromRoot from '../../../reducers';

@Component({
  selector: 'app-farm-select',
  templateUrl: './farm-select.component.html',
  styleUrls: ['./farm-select.component.scss']
})
export class FarmSelectComponent {
  farms$: Observable<Farm[]>;

  constructor(private store: Store<fromRoot.State>) {
    this.farms$ = this.store.pipe(select(fromRoot.getAllFarms));
  }
}
