import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as BarnActions from 'src/app/core/actions/barn.actions';
import * as FarmActions from 'src/app/core/actions/farm.actions';
import { Barn } from '../../../core/models/Barn';
import * as fromRoot from '../../../reducers';
import { RxjsComponent } from '../../../shared/classes/RxjsComponent';

@Component({
  selector: 'app-barns-overview',
  templateUrl: './barns-overview.component.html',
  styleUrls: ['./barns-overview.component.scss']
})
export class BarnsOverviewComponent extends RxjsComponent implements OnInit {
  barns$: Observable<Barn[]>;
  farmId: number;

  constructor(private store: Store<fromRoot.State>, private route: ActivatedRoute) {
    super();
    this.barns$ = this.store.pipe(select(fromRoot.getAllBarns));
  }

  ngOnInit(): void {
    this.addSubscription(
      this.route.params
        .pipe(
          map(params => {
            this.farmId = params.farmId;
            this.store.dispatch(new BarnActions.Reset());
            this.store.dispatch(new BarnActions.Fetch(this.farmId));
            return new FarmActions.Select(this.farmId);
          })
        )
        .subscribe(this.store)
    );
  }
}
