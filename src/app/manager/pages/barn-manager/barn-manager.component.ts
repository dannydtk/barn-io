import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import * as SectorActions from 'src/app/core/actions/sector.actions';
import * as WallpointActions from 'src/app/core/actions/wallpoint.actions';
import { RxjsComponent } from 'src/app/shared/classes/RxjsComponent';
import * as BarnActions from '../../../core/actions/barn.actions';
import * as FarmActions from '../../../core/actions/farm.actions';
import { Barn } from '../../../core/models/Barn';
import * as fromRoot from '../../../reducers';
import { BarnComponent } from '../../components/barn/barn.component';

@Component({
  selector: 'app-barn-manager',
  templateUrl: './barn-manager.component.html',
  styleUrls: ['./barn-manager.component.scss'],
  animations: [
    trigger('onHover', [
      state(
        'normal',
        style({
          transform: 'scale(1)',
          opacity: 1
        })
      ),
      state(
        'hovered',
        style({
          transform: 'scale(1.1)',
          opacity: 0.8
        })
      ),
      transition('normal <=> hovered', [animate('0.3s ease-in')])
    ])
  ]
})
export class BarnManagerComponent extends RxjsComponent implements OnInit {
  @ViewChild('barnComponent') barnComponent: BarnComponent;

  barn$: Observable<Barn>;
  barns$: Observable<Barn[]>;

  showFiller = false;
  clicked = false;

  constructor(private store: Store<fromRoot.State>, private route: ActivatedRoute) {
    super();
    this.barn$ = this.store.pipe(select(fromRoot.getSelectedBarn));
    this.barns$ = this.store.pipe(select(fromRoot.getAllBarns));
  }

  ngOnInit(): void {
    this.addSubscription(
      this.route.params
        .pipe(
          withLatestFrom(this.store.pipe(select(fromRoot.getSelectedFarmId))),
          map(([params, farmId]) => {
            if (!farmId) {
              this.store.dispatch(new FarmActions.Fetch());
              this.store.dispatch(new FarmActions.Select(params.farmId));
              this.store.dispatch(new BarnActions.Fetch(params.farmId));
            }
            this.store.dispatch(new WallpointActions.Reset());
            this.store.dispatch(new WallpointActions.Fetch(params.barnId));
            this.store.dispatch(new SectorActions.Reset());
            this.store.dispatch(new SectorActions.Fetch(params.barnId));
            return new BarnActions.Select(params.barnId);
          })
        )
        .subscribe(this.store)
    );
  }

  onClick(): void {
    this.clicked = !this.clicked;
  }

  onResetClick(): void {
    this.barnComponent.reset();
  }

  onSaveClick(): void {
    this.barnComponent.save();
  }
}
