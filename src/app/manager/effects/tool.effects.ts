import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ToolActionsUnion, ToolActionTypes } from '../actions/tool.actions';

@Injectable()
export class ToolEffects {
  @Effect({ dispatch: false })
  select$: Observable<string> = this.actions$.pipe(
    ofType<ToolActionsUnion>(ToolActionTypes.SELECT),
    map(action => action.payload)
  );

  constructor(private actions$: Actions) {}
}
