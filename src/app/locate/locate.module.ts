import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ngx-custom-validators';
import { MaterialModule } from '../material/material.module';
import { LocateRoutingModule } from './locate-routing.module';
import { LocateComponent } from './pages/locate/locate.component';

@NgModule({
  declarations: [LocateComponent],
  imports: [
    CommonModule,
    LocateRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    CustomFormsModule
  ]
})
export class LocateModule {}
