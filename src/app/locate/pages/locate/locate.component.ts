import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { CustomValidators } from 'ngx-custom-validators';
import { Observable } from 'rxjs';
import { catchError, map, take, withLatestFrom } from 'rxjs/operators';
import * as FarmActions from 'src/app/core/actions/farm.actions';
import { CowData } from 'src/app/core/models/Cow';
import { RxjsComponent } from 'src/app/shared/classes/RxjsComponent';
import * as BarnActions from '../../../core/actions/barn.actions';
import * as CowActions from '../../../core/actions/cow.actions';
import * as fromRoot from '../../../reducers';

@Component({
  selector: 'app-locate',
  templateUrl: './locate.component.html',
  styleUrls: ['./locate.component.scss']
})
export class LocateComponent extends RxjsComponent implements OnInit {
  cows$: Observable<CowData[]>;
  form: FormGroup;

  constructor(
    private store: Store<fromRoot.State>,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    super();
    this.cows$ = this.store.pipe(select(fromRoot.getAllCows));
    this.form = this.formBuilder.group({
      cowNumber: ['', [Validators.required, CustomValidators.number], cowNumberExists(this.cows$)]
    });
  }

  ngOnInit(): void {
    this.addSubscription(
      this.route.params
        .pipe(
          withLatestFrom(this.store.pipe(select(fromRoot.getSelectedFarmId))),
          map(([params, farmId]) => {
            if (!farmId) {
              this.store.dispatch(new FarmActions.Fetch());
              this.store.dispatch(new FarmActions.Select(params.farmId));
            }
            this.store.dispatch(new BarnActions.Fetch(params.farmId));
            return new CowActions.Fetch(params.farmId);
          })
        )
        .subscribe(this.store)
    );
  }

  submit(): void {
    this.cows$.pipe(take(1)).subscribe(cows => {
      const foundCow: CowData = cows.find(
        cow => cow.number === Number(this.form.get('cowNumber').value)
      );
      if (foundCow) {
        this.store.dispatch(new CowActions.Select(Number(foundCow.id)));
        this.store.dispatch(new CowActions.Locate(Number(foundCow.id)));
        return;
      }
    });
  }

  getErrorMessage(): string {
    if (this.form.get('cowNumber').hasError('cowNumberExists')) {
      return 'Krowa o podanym numerze nie należy do tej farmy';
    }
    if (this.form.get('cowNumber').hasError('required')) {
      return 'Podanie numeru krowy jest wymagane do lokalizacji';
    }
    if (this.form.get('cowNumber').hasError('number')) {
      return 'Wpisana wartość powinna być numerem';
    }
  }
}

function cowNumberExists(cows$: Observable<CowData[]>): AsyncValidatorFn {
  return (
    control: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return cows$.pipe(
      take(1),
      map(cows =>
        !cows.some(cow => cow.number === Number(control.value)) ? { cowNumberExists: true } : null
      ),
      catchError(() => null)
    );
  };
}
