import { NgModule } from '@angular/core';
import { ElementsHostDirective } from './elements-host.directive';

@NgModule({
  declarations: [ElementsHostDirective],
  exports: [ElementsHostDirective]
})
export class DirectivesModule {}
