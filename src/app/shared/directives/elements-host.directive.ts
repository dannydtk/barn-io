import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appElementsHost]'
})
export class ElementsHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
